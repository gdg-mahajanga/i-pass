import { signInWithEmailAndPassword, signOut } from "firebase/auth";
import { LocalStorageService } from "./localStorage";
import { auth } from "../firebase.config";

type FirebaseUser = {
    email: string;
    password: string;
};

export class AuthService {
    static isConnected = (): boolean => {
        return !!LocalStorageService.getData("token");
    };

    static getToken = (): string => {
        return (
            (this.isConnected() && LocalStorageService.getData("token")) || ""
        );
    };

    static logOut = (): void => {
        signOut(auth);
    };

    static logIn = ({ email, password }: FirebaseUser) => {
        return signInWithEmailAndPassword(auth, email, password);
    };
}
