export class LocalStorageService {
    static setData = (key: string, data: string) => {
        localStorage.setItem(key, data);
    };

    static getData = (key: string): string | null => {
        return localStorage.getItem(key);
    };

    static clearData = (key?: string): void => {
        if (key) {
            localStorage.removeItem(key);
        } else {
            localStorage.clear();
        }
    };
}
