import { Navigate, Outlet, useLocation } from "react-router-dom";
import { Suspense } from "react";
import { AuthService } from "../services/auth";
import DashboardLayout from "../layouts/DashboardLayout";
import LoginForm from "../pages/admin/login/Login";

const isConnected = AuthService.isConnected();

const PrivateRoute = () => {
    const { pathname } = useLocation();

    return (
        <>
            {!isConnected && pathname.startsWith("/admin") && <LoginForm />}
            {isConnected && pathname == "/admin" ? (
                <Navigate to="/admin/dashboard" />
            ) : (
                <DashboardLayout>
                    <Suspense>
                        <Outlet />
                    </Suspense>
                </DashboardLayout>
            )}
        </>
    );
};

export default PrivateRoute;
