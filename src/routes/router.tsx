import { lazy } from "react";
import {
    Route,
    RouterProvider,
    createBrowserRouter,
    createRoutesFromElements,
} from "react-router-dom";
import PrivateRoute from "./PrivateRoute";

const HomeScreen = lazy(() => import("../pages/client/home/Home"));
const DashboardScreen = lazy(
    () => import("../pages/admin/dashboard/Dashboard")
);
const LoginScreen = lazy(() => import("../pages/admin/login/Login"));
const NotFoundScreen = lazy(() => import("../components/NotFound/NotFound"));
const EventsScreen = lazy(() => import("../pages/admin/events/Events"));
const MembersScreen = lazy(() => import("../pages/admin/members/Members"));
const OrganizationScreen = lazy(
    () => import("../pages/admin/organization/Organization")
);

const AppRouter = () => {
    const router = createBrowserRouter(
        createRoutesFromElements(
            <>
                <Route
                    path="/"
                    element={<HomeScreen />}
                    errorElement={<NotFoundScreen />}
                />
                <Route path="/admin" element={<PrivateRoute />}>
                    <Route
                        index
                        element={<LoginScreen />}
                        errorElement={<NotFoundScreen />}
                    />
                    <Route
                        path="/admin/dashboard"
                        element={<DashboardScreen />}
                        errorElement={<NotFoundScreen />}
                    />
                    <Route
                        path="/admin/events"
                        element={<EventsScreen />}
                        errorElement={<NotFoundScreen />}
                    />
                    <Route
                        path="/admin/members"
                        element={<MembersScreen />}
                        errorElement={<NotFoundScreen />}
                    />
                    <Route
                        path="/admin/organization"
                        element={<OrganizationScreen />}
                        errorElement={<NotFoundScreen />}
                    />
                </Route>
            </>
        )
    );

    return <RouterProvider router={router}></RouterProvider>;
};

export default AppRouter;
