import { FC } from "react";

const Home: FC = () => {
    const APP_NAME = import.meta.env.VITE_REACT_APP_NAME;

    return (
        <div className="h-screen w-screen flex justify-center items-center">
            <h1 className="text-zinc-800 text-3xl">Site under construction</h1>
            <br />
            <p>App name: {APP_NAME}</p>
        </div>
    );
};

export default Home;
