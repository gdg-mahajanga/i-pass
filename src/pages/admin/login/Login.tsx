import { FC } from "react";
import { Formik, Field, ErrorMessage, Form } from "formik";
import * as Yup from "yup";
import { AuthService } from "../../../services/auth";
import { LocalStorageService } from "../../../services/localStorage";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { UserCredential } from "firebase/auth";

const LoginSchema = Yup.object().shape({
    email: Yup.string()
        .email("Invalid email address format")
        .required("Email is required"),
    password: Yup.string()
        .min(6, "Password must be 6 characters at minimum")
        .required("Password is required"),
});

const LoginForm: FC = () => {
    const navigate = useNavigate();

    const handleLoginSuccess = async (response: UserCredential) => {
        const token = await response.user.getIdToken();
        await LocalStorageService.setData("token", token);
        toast.success("You're logged in successfully", {
            position: "top-center",
            autoClose: 4000,
        });
        setTimeout(() => {
            navigate("/admin/dashboard");
            window.location.reload();
        }, 4000);
    };

    const handleLoginError = () => {
        /* const errorCode = error.code;
            const errorMessage = error.message; */

        // console.log(errorCode, errorMessage);
        toast.error("Invalid credentials", {
            position: "top-center",
        });
    };

    return (
        <section className="h-screen flex items-center">
            <div>
                <Formik
                    initialValues={{ email: "", password: "" }}
                    validationSchema={LoginSchema}
                    onSubmit={(values, { setSubmitting }) => {
                        setSubmitting(true);
                        AuthService.logIn({
                            email: values.email,
                            password: values.password,
                        })
                            .then(handleLoginSuccess)
                            .catch(handleLoginError)
                            .finally(() => setSubmitting(false));
                    }}
                >
                    {(props) => (
                        <div className="w-[300px]">
                            <Form>
                                <header>
                                    <h2>Login</h2>
                                </header>
                                <div>
                                    <label htmlFor="email">Email</label>
                                    <Field
                                        type="email"
                                        name="email"
                                        placeholder="Enter email"
                                        autoComplete="on"
                                        className={`my-3 ${
                                            props.touched.email &&
                                            props.errors.email
                                                ? "border-2 border-red-500"
                                                : ""
                                        }`}
                                    />
                                    <ErrorMessage
                                        component="div"
                                        name="email"
                                        className="text-red-500"
                                    />
                                </div>
                                <div>
                                    <label htmlFor="password" className="mt-3">
                                        Password
                                    </label>
                                    <Field
                                        type="password"
                                        name="password"
                                        placeholder="Enter password"
                                        className={`my-3 ${
                                            props.touched.password &&
                                            props.errors.password
                                                ? "border-2 border-red-500"
                                                : ""
                                        }`}
                                    />
                                    <ErrorMessage
                                        component="div"
                                        name="password"
                                        className="text-red-500"
                                    />
                                </div>
                                <button
                                    type="submit"
                                    className="bg-blue-500 p-2"
                                    disabled={props.isSubmitting}
                                >
                                    Sign In
                                </button>
                            </Form>
                        </div>
                    )}
                </Formik>
            </div>
        </section>
    );
};

export default LoginForm;
