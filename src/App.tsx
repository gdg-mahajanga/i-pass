import { Suspense } from "react";
import AppRouter from "./routes/router";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

function App() {
    return (
        <Suspense fallback={<p>Loading App...</p>}>
            <ToastContainer />
            <AppRouter />
        </Suspense>
    );
}

export default App;
