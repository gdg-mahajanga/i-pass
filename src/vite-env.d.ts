/// <reference types="vite/client" />

declare global {
    interface ImportMetaEnv {
        readonly VITE_REACT_APP_NAME: string;
        readonly VITE_REACT_APP_PROJECT_ID: string;
        readonly VITE_REACT_APP_API_KEY: string;
        readonly VITE_REACT_APP_MESSAGING_SENDER_ID: string;
        readonly VITE_REACT_APP_APP_ID: string;
        readonly VITE_REACT_APP_MEASUREMENT_ID: string;
        readonly VITE_REACT_APP_DATABASE_URL: string;
    }
}
