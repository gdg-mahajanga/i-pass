import { FC, Suspense } from "react";
import { Link, useNavigate } from "react-router-dom";
import { AuthService } from "../services/auth";
import { toast } from "react-toastify";
import { LocalStorageService } from "../services/localStorage";

type Props = {
    children: JSX.Element;
};

const DashboardLayout: FC<Props> = ({ children }) => {
    const navigate = useNavigate();

    const handleLogOut = async () => {
        await AuthService.logOut();
        await LocalStorageService.clearData("token");
        toast.success("You're logged out successfully", {
            position: "top-center",
            autoClose: 4000,
        });
        setTimeout(() => {
            navigate("/admin");
            window.location.reload();
        }, 4000);
    };

    return (
        <div className="flex relative">
            <section className="w-[300px] h-screen flex flex-col justify-start p-6 bg-gray-200 sticky">
                <div>
                    <h1 className="text-center text-2xl py-12">i-Pass</h1>
                </div>
                <div className="flex flex-col">
                    <Link to="/admin/dashboard">Dashboard</Link>
                    <Link to="/admin/events">Events</Link>
                    <Link to="/admin/members">Members</Link>
                    <Link to="/admin/organization">Organization</Link>
                </div>
                <div className="absolute bottom-8">
                    <button onClick={handleLogOut}>Log out</button>
                </div>
            </section>
            <section className="w-full h-full bg-gray-700 text-white">
                <Suspense>{children}</Suspense>
            </section>
        </div>
    );
};

export default DashboardLayout;
