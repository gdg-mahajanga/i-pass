import { FC } from "react";
import { Link } from "react-router-dom";

const NotFound: FC = () => {
    return (
        <main className="h-screen w-screen flex flex-col justify-center items-center">
            <h1 className="text-zinc-800 text-2xl">Oups! Page not found</h1>
            <p className="text-zinc-600">
                Please, go to{" "}
                <Link to="/" className="text-blue-500 font-bold">
                    homepage
                </Link>
            </p>
        </main>
    );
};

export default NotFound;
