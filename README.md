# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

-   [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
-   [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

-   Configure the top-level `parserOptions` property like this:

```js
export default {
    // other rules...
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
        project: ["./tsconfig.json", "./tsconfig.node.json"],
        tsconfigRootDir: __dirname,
    },
};
```

-   Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
-   Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
-   Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list

## Environment variables

Create 3 `.env` files:

-   `.env.development`
-   `.env.test`
-   `.env.production`

Inside `.env.development`, add the following line:

```js
VITE_REACT_APP_PROJECT_ID=""
VITE_REACT_APP_NAME=""
VITE_REACT_APP_API_KEY=""
VITE_REACT_APP_MESSAGING_SENDER_ID=""
VITE_REACT_APP_APP_ID=""
VITE_REACT_APP_MEASUREMENT_ID=""
VITE_REACT_APP_DATABASE_URL="",

```

Repeat and change for other `.env` files.

## Deployment

### For development environment

-   Hosting URL: https://i-pass-dev.web.app

### For production environment

-   Hosting URL: https://ipass-acm.web.app

### For test environment

-   Hosting URL: https://i-pass-test.web.app
